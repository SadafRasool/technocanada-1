package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Home 
{
	WebDriver localDriver;
	
	public Home(WebDriver driver) 
	{
		 localDriver = driver;
	}
	
	@FindBy(how=How.XPATH, using="(//a[@href='https://www.technotutors.ca'])[1]/img")
	public WebElement homePageLogo; 
	
	@FindBy(how=How.LINK_TEXT, using="Home")
	public WebElement linkHome; 
	
	@FindBy(how=How.LINK_TEXT, using="About Us")
	public WebElement linkaboutUs; 
	
	@FindBy(how=How.LINK_TEXT, using="Facility")
	public WebElement linkFacility; 
	
	@FindBy(how=How.LINK_TEXT, using="Subject Areas")
	public WebElement linkSubjectAreas; 
	
	@FindBy(how=How.LINK_TEXT, using="Contact")
	public WebElement linkContact; 
	
	@FindBy(how=How.LINK_TEXT, using="Cart")
	public WebElement linkCart; 
	
	@FindBy(how=How.XPATH, using="//h2[text()='Subjects']")
	public WebElement lblSubjectsSection; 
	
	@FindBy(how=How.XPATH, using="//a[@href='/subject-areas/']//parent::div[@class='vc_btn3-container  math-btn vc_btn3-right']/a")
	public WebElement btnMathMore; 
	
	@FindBy(how=How.XPATH, using="//div[@class='vc_btn3-container  eng-btn vc_btn3-right']/a")
	public WebElement btnEnglishMore; 
	
	@FindBy(how=How.XPATH, using="//div[@class='vc_btn3-container  fr-btn vc_btn3-right']/a")
	public WebElement btnFrenchMore; 
	
	@FindBy(how=How.XPATH, using="//h2[text()='CONTACT US']")
	public WebElement lblContactUSSection; 
	
	@FindBy(how=How.XPATH, using="//h2[text()='BOOK A TUTOR NOW']")
	public WebElement lblBookTutor; 
    
	@FindBy(how=How.XPATH, using="//input[@name='text-625']")
	public WebElement txtName; 
	
	@FindBy(how=How.XPATH, using="//input[@name='email-982']")
	public WebElement txtEmail; 
	
	@FindBy(how=How.XPATH, using="//input[@name='text-org']")
	public WebElement txtSubject; 
	
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']")
	public WebElement txtMessage; 
	
	@FindBy(how=How.XPATH, using="//input[@value='Send']")
	public WebElement btnSend;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap text-625']/span")
	public WebElement ERMessageName;
		
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap email-982']/span")
	public WebElement ERMessageEmail;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap text-org']/span")
	public WebElement ERMessageSubject;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap textarea-841']/span")
	public WebElement ErrMessageMessage; 
	
	@FindBy(how=How.XPATH, using="//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-validation-errors']")
	public WebElement ErrMessageSend; 
	
	@FindBy(how=How.XPATH, using="//div[@class='tp-leftarrow tparrows uranus  noSwipe']")
	public WebElement linkLeftArrow; 
	
	@FindBy(how=How.XPATH, using="//div[@class='tp-rightarrow tparrows uranus  noSwipe']")
	public WebElement linkRightArrow; 
	
	@FindBy(how=How.XPATH, using="//h1[text()='Techno Tutors ']")
	public WebElement JSlblTechnoTutors; 
	
	@FindBy(how=How.XPATH, using="//strong[contains(text(),' will help your children')]")
	public WebElement JSlblMovingText; 
	
	@FindBy(how=How.XPATH, using="//div[text()='Education ']")
	public WebElement JSlblEducation; 
	
	@FindBy(how=How.XPATH, using="//div[text()='Learning Courses ']")
	public WebElement JSlblLearnCourses; 
	
	@FindBy(how=How.XPATH, using="//a[text()='ABOUT US']")
	public WebElement JSlinkAbout; 
	
	@FindBy(how=How.XPATH, using="//a[text()='SUBJECTS']")
	public WebElement JSlinkSubjects; 
	
	@FindBy(how=How.XPATH, using="//div[contains(text(),'Thank you for your message')]")
	public WebElement lblSuccessMessage;
	
	public String HomePageTitle() 
	{
		// HomeTechno Tutors � Personalized Tutoring Service
		return localDriver.getTitle();
	}
	
}




