package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class AboutUs 
{
	
	WebDriver localDriver;
	
	public AboutUs(WebDriver driver) 
	{
		 
		 localDriver = driver;
	}
		
	@FindBy(how=How.XPATH, using="//button[@class='btn btn-primary']")
	public WebElement btnContactUS; 
	
	@FindBy(how=How.XPATH, using="//div[@class='avgrund-content']/h3")
	public WebElement lblContactUS; 

	@FindBy(how=How.XPATH, using="//input[@name='text-625']")
	public WebElement txtName; 
	
	@FindBy(how=How.XPATH, using="//input[@name='email-982']")
	public WebElement txtEmail; 
	
	@FindBy(how=How.XPATH, using="//input[@name='text-org']")
	public WebElement txtSubject; 
	
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']")
	public WebElement txtMessage; 
	
	@FindBy(how=How.XPATH, using="//input[@value='Send']")
	public WebElement btnSend;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap text-625']/span")
	public WebElement ERMessageName;
		
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap email-982']/span")
	public WebElement ERMessageEmail;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap text-org']/span")
	public WebElement ERMessageSubject;
	
	@FindBy(how=How.XPATH, using="//span[@class='wpcf7-form-control-wrap textarea-841']/span")
	public WebElement ErrMessageMessage; 
	
	@FindBy(how=How.XPATH, using="//img[@alt='close']")
	public WebElement imgClose;
	public String AboutUsPageTitle() 
	{
		// TODO Auto-generated method stub
		return localDriver.getTitle();
	} 
	
	
}
